cmake_minimum_required(VERSION 3.16)

project(mist)

include(${CMAKE_BINARY_DIR}/conan_paths.cmake)
#include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
#conan_basic_setup()
#conan_set_vs_runtime()

add_subdirectory(src)