#include <iostream>
#include <array>

template<typename T, std::size_t N>
struct MultiIndexer
{
    static_assert(std::is_integral_v<T>, "Index limits must be an integral type");

    public:
    using IndexLimitsArrayType = std::array<T, N>;

    MultiIndexer(const IndexLimitsArrayType& lowerLims,
        const IndexLimitsArrayType& upperLims) : lowerLimitsArray{lowerLims},
        upperLimitsArray{upperLims}{}

    MultiIndexer(const IndexLimitsArrayType& upperLims) : upperLimitsArray{upperLims}
    {
        std::fill(lowerLimitsArray.begin(), lowerLimitsArray.end(), 0);
    }

    class iterator
    {
        const IndexLimitsArrayType& upperLimits;
        const IndexLimitsArrayType& lowerLimits;
        IndexLimitsArrayType indcs;  
        bool _end{false};

        public:
        struct sentinal_t {};

        explicit iterator(const IndexLimitsArrayType& lowerLims,
            const IndexLimitsArrayType& upperLims) : lowerLimits{lowerLims},
                upperLimits{upperLims}, indcs{lowerLims}{}

        auto& operator++()
        {
            for (int i = 0; i < N; ++i){
                if (indcs[i] < upperLimits[i]){
                    ++indcs[i];

                    for (int j = 0; j < i; ++j){
                        indcs[j] = lowerLimits[j];
                    }

                    return *this;
                }
            }

            _end = true;
            return *this;
        }

        auto& operator*()
        {
            return indcs;
        }

        bool operator!=(sentinal_t) const 
        {
            return !_end;
        }
    };

    auto begin() const
    {
        return iterator{lowerLimitsArray, upperLimitsArray};
    }

    auto end() const
    {
        return typename iterator::sentinal_t{};
    }

    private:
    IndexLimitsArrayType lowerLimitsArray;
    IndexLimitsArrayType upperLimitsArray;
};