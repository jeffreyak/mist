#define _USE_MATH_DEFINES

#ifndef ftFacet_h
#define ftFacet_h

#include <Eigen/Dense>
#include <CGAL/circulator.h>
#include <boost/range/combine.hpp>

#include <cmath>
#include <complex>
#include <vector>
#include <array>

class FTFacet
{
private:
	using VectorType = Eigen::Vector3d;

	std::vector<VectorType> m_EdgeMidpoints;
	std::vector<VectorType> m_EdgeTangents;
	std::vector<VectorType> m_EdgeNormals;
	double m_FacetArea;

public:
	using KSpaceVectorType = Eigen::Vector3d;

	template<typename TFacet>
	FTFacet(const TFacet& facet) : m_FacetArea{ 0.0 }
	{
		// Cache the quantities we'll need for calculating every
		// k-space point
		auto facetDegree = facet.facet_degree();
		m_EdgeMidpoints.resize(facetDegree);
		m_EdgeTangents.resize(facetDegree);
		m_EdgeNormals.resize(facetDegree);
		
		VectorType facetNormal{
			facet.plane()[0],
			facet.plane()[1],
			facet.plane()[2]
		};
		facetNormal.normalize();

		auto facetCirculator = facet.facet_begin();
		auto edgeMidpointIt = m_EdgeMidpoints.begin();
		auto edgeTangentIt = m_EdgeTangents.begin();
		auto edgeNormalIt = m_EdgeNormals.begin();

		for (; edgeMidpointIt != m_EdgeMidpoints.end();
			 ++facetCirculator, ++edgeMidpointIt, ++edgeTangentIt, ++edgeNormalIt) {
			VectorType vertexPoint{
						facetCirculator->vertex()->point()[0],
						facetCirculator->vertex()->point()[1],
						facetCirculator->vertex()->point()[2]
					};
			VectorType nextVertexPoint{
				facetCirculator->next()->vertex()->point()[0],
				facetCirculator->next()->vertex()->point()[1],
				facetCirculator->next()->vertex()->point()[2]
			};

			*edgeMidpointIt = 0.5 * (vertexPoint + nextVertexPoint);
			*edgeTangentIt = (nextVertexPoint - vertexPoint);
			*edgeNormalIt = (*edgeTangentIt).cross(facetNormal);

			m_FacetArea += facetNormal.dot(vertexPoint.cross(nextVertexPoint));
		}
		
		m_FacetArea = 0.5 * std::abs(m_FacetArea);
	}

	std::complex<double> operator()(const KSpaceVectorType& kspaceVector)
	{
		auto kNorm = kspaceVector.norm();
		double besselArgument{ 0.0 };
		std::complex<double> sumTerm{ 0.0 };
		std::complex<double> exponentialTerm{ 0.0 };

		if (kNorm < 1.0e+1 * std::numeric_limits<double>::min()) {
			return m_FacetArea;
		}

		for (std::size_t i = 0; i < m_EdgeNormals.size(); ++i) {
			exponentialTerm = std::exp(-2.0 * M_PI * std::complex<double>(0, 1.0) * kspaceVector.dot(m_EdgeMidpoints[i]));
			besselArgument = std::abs(M_PI * kspaceVector.dot(m_EdgeTangents[i]));
			auto besselFcn = std::sph_bessel(0, besselArgument);
			auto firstTerm = kspaceVector.dot(m_EdgeNormals[i]);
			sumTerm += kspaceVector.dot(m_EdgeNormals[i]) * std::sph_bessel(0, besselArgument) * exponentialTerm;
		}

		return sumTerm * std::complex<double>(0.0, 1.0) / (
			2.0 * M_PI * std::pow(kNorm, 2.0));
	}
};

#endif