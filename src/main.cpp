#define _USE_MATH_DEFINES

#include "MultiIndexer.h"
#include "FTFacet.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_traits_with_normals_3.h>
#include <CGAL/algorithm.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>

#include <Eigen/Dense>

#include <itkImage.h>
#include <itkImageRegionIterator.h>
#include <itkInverseFFTImageFilter.h>
#include <itkFFTShiftImageFilter.h>
#include <itkNrrdImageIO.h>
#include <itkImageFileWriter.h>


#include <iostream>
#include <array>
#include <algorithm>
#include <cmath>
#include <complex>

using K = CGAL::Exact_predicates_inexact_constructions_kernel;
using Point_2 = K::Point_2;
using Polygon_2 = CGAL::Polygon_2<K>;
using Point_3 = K::Point_3;
using Traits = CGAL::Polyhedron_traits_with_normals_3<K>;
using Polyhedron_3 = CGAL::Polyhedron_3<Traits>;

using KSpaceCoefficientsArrayType = std::vector<std::complex<double>>;

template<std::size_t NDim>
using KSpaceTrajectoryType = std::vector<Eigen::Matrix<double, NDim, 1>>;

template<typename T, std::size_t NDim>
auto generateCartesianSamplingTrajectory(const std::array<double, NDim>& fieldOfView,
										 const std::array<T, NDim>& gridDims) -> 
	KSpaceTrajectoryType<NDim>
{
	static_assert(std::is_integral_v<T>, "Grid dimensions must be an integral type");
	std::array<double, NDim> sampleSpacing;
	std::array<double, NDim> offsets;

	std::transform(fieldOfView.begin(), fieldOfView.end(), sampleSpacing.begin(),
				   [](const auto& fovDim) { return 1.0 / fovDim; });

	std::transform(gridDims.begin(), gridDims.end(), sampleSpacing.begin(), offsets.begin(), [](
		const auto& gridDim, const auto& spacing) {
		return spacing * static_cast<double>(std::floor(gridDim / 2));
	});

	KSpaceTrajectoryType<NDim> kSpaceTrajectory;
	std::array<T, NDim> indexDims;
	std::transform(gridDims.begin(), gridDims.end(), indexDims.begin(),
				   [](const auto& dim) {
		return dim - 1;
	});

	for (const auto& indcs : MultiIndexer(indexDims)) {
		//std::cout << "(" << indcs[0] << ", " << indcs[1] << ")" << std::endl;
		KSpaceTrajectoryType<NDim>::value_type kSpacePoint;
		for (unsigned int i = 0; i < NDim; ++i) {
			kSpacePoint[i] = indcs[i] * sampleSpacing[i] - offsets[i];
		}
		kSpaceTrajectory.push_back(kSpacePoint);
	}

	return kSpaceTrajectory;
}

KSpaceCoefficientsArrayType fourierTransform(const Polygon_2& polygon,
												   const KSpaceTrajectoryType<2>& trajectory)
{
	KSpaceCoefficientsArrayType coeffs;

	// Precompute some quantities we'll need for every k-space point
	using VectorType = Eigen::Matrix<double, 2, 1>;
	std::vector<VectorType> cn, vertex_diff;
	cn.resize(polygon.size());
	vertex_diff.resize(polygon.size());
	std::vector<Eigen::Matrix<double, 3, 1>> ln;
	ln.resize(polygon.size());

	auto vertexCirculator = polygon.vertices_circulator();
	++vertexCirculator;

	auto vertexDiffIt = vertex_diff.begin();
	auto cnIt = cn.begin();
	auto lnIt = ln.begin();

	for (auto vertexIt = polygon.vertices_begin(); vertexIt != polygon.vertices_end(); ++vertexIt) {
		VectorType v1;
		VectorType v2;

		// Convert to Eigen types
		for (unsigned int i = 0; i < VectorType::SizeAtCompileTime; ++i) {
			v1[i] = (*vertexIt).cartesian(i);
			v2[i] = (*vertexCirculator).cartesian(i);
		}
		
		*vertexDiffIt = v2 - v1;
		*cnIt = 0.5 * (v2 + v1);

		(*lnIt).block<2, 1>(0, 0) = *vertexDiffIt;
		*lnIt = (*lnIt).cross(Eigen::Matrix<double, 3, 1>{0.0, 0.0, 1.0});

		++cnIt;
		++lnIt;
		++vertexDiffIt;
		++vertexCirculator;
	}

	for (const auto& kSpacePoint : trajectory) {	
		std::complex<double> sumTerm{ 0.0 };
		std::complex<double> exponentialTerm{ 0.0 };
		double besselArgument{ 0.0 };
		auto kNorm = kSpacePoint.norm();

		// Handle k == 0 as a special case
		if (kNorm < 100.0 * std::numeric_limits<double>::min()) {
			std::cout << "Found k == 0; norm(k) == " << kNorm << std::endl;
			coeffs.push_back(std::abs(polygon.area()));

			continue;
		}

		for (std::size_t i = 0; i < polygon.size(); ++i) {
			exponentialTerm = std::exp(-2.0 * M_PI * std::complex<double>(0, 1.0) * kSpacePoint.dot(cn[i]));
			besselArgument = std::abs(M_PI * kSpacePoint.dot(vertex_diff[i]));
			auto besselFcn = std::sph_bessel(0, besselArgument);
			auto firstTerm = kSpacePoint.dot(ln[i].block<2, 1>(0, 0));
			sumTerm += kSpacePoint.dot(ln[i].block<2, 1>(0, 0)) * std::sph_bessel(0, besselArgument) * exponentialTerm;
		}

		std::complex<double> multiplier = std::complex<double>(0.0, 1.0) / (
			2.0 * M_PI * std::pow(kNorm, 2.0));

		coeffs.push_back(multiplier * sumTerm);
	}

	return coeffs;
}

template<class HDS>
class BuildThing : public CGAL::Modifier_base<HDS> {
public:
	BuildThing() = default;
	void operator()(HDS& hds) {
		CGAL::Polyhedron_incremental_builder_3<HDS> B(hds, true);
		B.begin_surface(5, 1);
		using Vertex = typename HDS::Vertex;
		using Point = typename Vertex::Point;
		B.add_vertex(Point(-1.0, -1.0, 0.0));
		B.add_vertex(Point(1.0, -1.0, 0.0));
		B.add_vertex(Point(1.0, 1.0, 0.0));
		B.add_vertex(Point(0.0, 0.5, 0.0));
		B.add_vertex(Point(-1.0, 1.0, 0.0));
		B.begin_facet();
		B.add_vertex_to_facet(0);
		B.add_vertex_to_facet(1);
		B.add_vertex_to_facet(2);
		B.add_vertex_to_facet(3);
		B.add_vertex_to_facet(4);
		B.end_facet();
		B.end_surface();
	}
};

struct NormalComputer {
	template<class Facet>
	typename Facet::Plane_3 operator()(Facet& f) {
		typename Facet::Halfedge_handle h = f.halfedge();
		return CGAL::cross_product(
			h->next()->vertex()->point() - h->vertex()->point(),
			h->next()->next()->vertex()->point() - h->next()->vertex()->point());
	}
};

auto main(int argc, const char* argv[]) -> int
{
	Polyhedron_3 p;
	BuildThing<Polyhedron_3::HalfedgeDS> thing;
	p.delegate(thing);

	std::transform(p.facets_begin(), p.facets_end(), p.planes_begin(),
				   NormalComputer());

	std::array<double, 3> fov{ 5.0, 5.0, 0.0};
	std::array<int, 3> gridDims{64, 64, 1};
	auto traj = generateCartesianSamplingTrajectory(fov, gridDims);

	for (auto& elem : traj) {
		elem[2] = 0.0;
		std::cout << "(" << elem.transpose() << ")" << std::endl;
	}

	//auto f = *faceIt;
	//auto degree = f.facet_degree();
	//auto n = f.plane();
	//auto facetIt = faceIt->facet_begin(); // halfedge around facet circulator
	//auto v = facetIt->vertex(); // starting vertex of halfedge
	//auto p = v->point(); // point3 corresponding to vertex
	
	//Polygon_2 p;
	//p.push_back({ -1.0, -1.0 });
	//p.push_back({ 1.0, -1.0 });
	//p.push_back({ 1.0, 1.0 });
	//p.push_back({ 0.0, 0.5 });
	//p.push_back({ -1.0, 1.0 });

	//std::cout << "Is polygon simple? " << std::boolalpha
	//	<< p.is_simple() << std::endl;

	//std::array<double, 2> fov{ 5.0, 5.0 };
	//std::array<int, 2> gridDims{64, 64};

	//std::cout << "generating sampling trajectory..." << std::endl;
	//auto traj = generateCartesianSamplingTrajectory(fov, gridDims);

	std::cout << "computing forward fourier transform..." << std::endl;
	std::vector<std::complex<double>> coeffs;
	//auto coeffs = fourierTransform(p, traj);
	for (const auto& kspacePoint : traj) {
		for (auto facetIt = p.facets_begin(); facetIt != p.facets_end(); ++facetIt) {
			FTFacet ft(*facetIt);
			coeffs.push_back(ft(kspacePoint));
			std::cout << coeffs.back() << std::endl;
		}
	}

	using ComplexImageType = itk::Image<std::complex<float>, 2>;
	using RealImageType = itk::Image<float, 2>;
	using IteratorType = itk::ImageRegionIterator<ComplexImageType>;

	auto kSpaceImage = ComplexImageType::New();
	ComplexImageType::RegionType region;
	ComplexImageType::SizeType regionSize;
	regionSize[0] = gridDims[0];
	regionSize[1] = gridDims[1];
	region.SetSize(regionSize);
	region.SetIndex({ 0, 0 });

	kSpaceImage->SetRegions(region);
	kSpaceImage->Allocate();
	std::cout << "ITK kspace image contains " <<
		kSpaceImage->GetLargestPossibleRegion().GetNumberOfPixels() 
		<< " pixels" << std::endl;

	std::cout << "kspace trajectory has " << traj.size() << " points" << std::endl;
	std::cout << "kspace coeffs has " << traj.size() << " points" << std::endl;

	IteratorType outputIt(kSpaceImage, region);
	outputIt.GoToBegin();

	std::cout << "filling ITK image with kspace coefficients..." << std::endl;
	for (const auto& k : coeffs) {
		outputIt.Set(k);
		++outputIt;
	}

	using FFTShiftFilterType = itk::FFTShiftImageFilter<ComplexImageType, ComplexImageType>;
	auto fftShiftFilter = FFTShiftFilterType::New();
	fftShiftFilter->SetInput(kSpaceImage);
	
	using IFFTFilterType = itk::InverseFFTImageFilter<ComplexImageType, RealImageType>;
	auto ifftFilter = IFFTFilterType::New();
	ifftFilter->SetInput(fftShiftFilter->GetOutput());

	std::cout << "Computing inverse FFT..." << std::endl;
	try {
		ifftFilter->Update();
	}
	catch (itk::ExceptionObject & e) {
		std::cerr << e.what() << std::endl;
	}

	using ShiftFilterType = itk::FFTShiftImageFilter<RealImageType, RealImageType>;
	auto shiftFilter = ShiftFilterType::New();
	shiftFilter->SetInput(ifftFilter->GetOutput());
	try {
		shiftFilter->Update();
	}
	catch (itk::ExceptionObject & e) {
		std::cerr << e.what() << std::endl;
	}
	// Write the image data
	std::string outputImageFileName{ "kSpaceRecon.nrrd" };

	auto nrrdImageIO = itk::NrrdImageIO::New();
	using WriterType = itk::ImageFileWriter<RealImageType>;
	auto writer = WriterType::New();
	writer->SetImageIO(nrrdImageIO);
	writer->SetInput(shiftFilter->GetOutput());
	writer->SetFileName(outputImageFileName);

	std::cout << "Writing output..." << std::endl;
	try
	{
		writer->Update();
	}
	catch (itk::ExceptionObject & e)
	{
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	

	return EXIT_SUCCESS;
}